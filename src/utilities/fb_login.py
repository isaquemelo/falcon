from browser_set import close


def login_verify(browser, domain):
    count = 0
    def is_logged():
        email_field = '#login_form input#email'
        pass_field = '#login_form input#pass'
        submit_button_id = 'loginbutton'
        next_step_button = 'button._54k8'
        print("Verifying login status...")
        browser.get(domain)

        try:
            browser.find_element_by_css_selector(email_field)
            browser.find_element_by_css_selector(pass_field)
            browser.find_element_by_id(submit_button_id)
            print("Login status: Unlogged\n")
            return False

        except:
            print("\nLogin status: Logged\n")
            return True

    def fb_login(username, password):
        email_field = 'input#email'
        pass_field = 'input#pass'
        submit_button_id = 'loginbutton'
        next_step_button = 'button._54k8'

        browser.get(domain)
        browser.find_element_by_css_selector(email_field).send_keys(username)
        print("Trying to login...")
        try:
            browser.find_element_by_css_selector(pass_field).send_keys(password)
            browser.find_element_by_id(submit_button_id).submit()
            print("Normal login has been runned.")

        except:
            print("Normal login has failed. Alternative is being runned.")
            browser.find_element_by_css_selector(next_step_button).submit()
            browser.find_element_by_css_selector(pass_field).send_keys(password)
            browser.find_element_by_id(submit_button_id).submit()

        print("Successful login...\n")
        return True

    while not is_logged():
        count += 1
        verifier = fb_login('isaquegabryel', '')
        if count >= 2:
            print("Login has failed more than 2 times. Verify fb_login() and try again.")
            close(browser)
            exit("The program wont be able of execute the next lines. The program was closed for avoiding errors.")
            break
        if verifier:
            print("\nLogin status: Logged\n")
            break

        print("Tries:", count)