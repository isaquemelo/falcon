# GOES TO THE RECEIVED URL
def go_to(browser, url):
    print("Going to %s"%url)
    browser.get(url)


def date_conversor(date):
    # print(date)
    date = date.split()
    month = date[0]
    day = date[1]
    try:
        year = date[2]
    except IndexError:
        # print("\nGet birthdate[year] has failed, this info sens to be private.\n")
        year = "0000"

    final_date = ""
    month = month.lower()
    months = {'january': 1, 'february': 2, 'march': 3, 'april': 4, 'may': 5,  'june': 6,  'july': 7,  'august': 8, 'september': 9, 'october': 10, 'november': 11, 'december': 12}
    final_date = "{}".format(year + '-' + '%s' % months[month] + '-' + day).replace(',','')
    return final_date


def gender_conversor(gender):
    # print(gender)
    if gender == "Female": return "f"
    elif gender == "Male": return "m"
    else: return "i"


def interest_conversor(gender):
    # print(gender)
    if gender == "Women": return "w"
    elif gender == "Men": return "m"
    else: return "i"

def exists(word,sentence):
    if word in sentence:
        return True
    else:
        return False