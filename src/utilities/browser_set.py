from selenium import webdriver


def open_browser():
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.add_argument("user-data-dir=C:\\profiles\\temp_profile")
    browser = webdriver.Chrome(chrome_options=chrome_options)
    return browser


def close(browser):
    browser.close()
    browser.quit()
    print("Success closing the browser.")