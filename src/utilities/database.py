import pymysql

host = "localhost"
user = "root"
password = ""
db = "datamining"


def db_connect():
    datasource = pymysql.connect(host=host, user=user, password=password, db=db)
    return datasource
