from selenium.webdriver.common.keys import Keys
from bs4 import *
from time import sleep


from utilities.multitool import *
from utilities.database import *
from utilities.fb_login import login_verify
from utilities.browser_set import *


domain = 'https://www.facebook.com'
'''
USER COLLECTOR IS ABLE TO GET:
- gender
- birthday
- family
- hometown
- current city
- interest
- religious
- languages
- relationship (returns None if empty)

'''




def user_collector(browser, fb_id):
    user_id = 1
    # login_verify(browser, domain)
    about_url = domain + '/%s' % fb_id + '/about?&section='

    def birthdate():
        birthdate = 'January 1 2000'
        try:

            '''
            BIRTHDAY VERSION 1
            if browser.current_url != about_url + 'about':
                go_to(browser, about_url + 'about')

            print("\nBirthdate mining has been started.")
            about_session = browser.find_element_by_css_selector('.uiList').get_attribute('innerHTML')
            about_session_soup = BeautifulSoup(about_session, 'html.parser')
            query = about_session_soup.find_all('li', class_='_4tnv')[-1].select('._c24')

            print('Catching user birthdate by method 1:')
            # BIRTHDATE &section=year-overviews
            birthdate = query[0].contents[1].contents[0]
            
            '''
            if browser.current_url != about_url + 'contact-info':
                go_to(browser, about_url + 'contact-info')

            print("\nBirthdate mining has been started.")
            about_session = browser.find_element_by_css_selector('#pagelet_basic .uiList').get_attribute('innerHTML')
            about_session_soup = BeautifulSoup(about_session, 'html.parser')
            query = about_session_soup.find_all('li', class_='_3pw9')

            for li in query:
                if li.select('._50f4')[0].contents[0] == 'Birthday':
                    birthdate = li.select('._2iem')[0].contents[0]

        except:
            print("Method 1 has failed. Executing alternative measure...")
            go_to(browser, about_url + 'contact-info')
            about_session = browser.find_element_by_css_selector('._4ms4 div #pagelet_basic ._4qm1').get_attribute('innerHTML')
            about_session_soup = BeautifulSoup(about_session, 'html.parser')
            query = about_session_soup.find_all('div', class_='_ikh')

            print('Catching user birthdate by method 2:')
            # BIRTHDATE &section=year-overviews
            birthdate = query[0].contents[1].contents[0].contents[0].contents[0].contents[0]

        birthdate = date_conversor(birthdate)
        print(birthdate)

    def family():
        go_to(browser, about_url + "relationship")
        relations_session = browser.find_element_by_css_selector('div[data-referrer="pagelet_relationships"]').get_attribute('innerHTML')

        def relationship():
            about_session_soup = BeautifulSoup(relations_session, 'html.parser')
            query = about_session_soup.select('._4bl9 span._c24')

        def family():
            print("\nFamily data mining has been started.")
            about_session_soup = BeautifulSoup(relations_session, 'html.parser')
            try:
                query = about_session_soup.select('div[data-referrer="family-relationships-pagelet"] ._4qm1 ._4kg')[0].find_all('li', class_='_43c8')
            except:
                print("The user hasn't any family info to be shown.")
                return


            print()

            for liItem in query:
                name_and_type = liItem.find_all('div', class_="fsm")
                try:
                    print("Verifying the user-link...")
                    name = name_and_type[0].select('span a')[0].contents[0]
                    print("Success.")

                except:
                    print("The user has not a link. Running alternative measure...")
                    name = name_and_type[0].select('span')[0].contents[0]
                    print("Alternative measure has been runned with success.")

                relation_type = name_and_type[1].contents[0].lower()

                print(relation_type + ':', name)
                print()


        family()

    def gender():
        if browser.current_url != about_url + 'contact-info':
            go_to(browser, about_url + 'contact-info')

        print("\nGender mining has been started.")
        about_session = browser.find_element_by_css_selector('#pagelet_basic .uiList').get_attribute('innerHTML')
        about_session_soup = BeautifulSoup(about_session, 'html.parser')
        query = about_session_soup.find_all('li', class_='_3pw9')
        for li in query:
            if li.select('._50f4')[0].contents[0] == 'Gender':
                gender = gender_conversor(li.select('._2iem')[0].contents[0])
                print(gender)

    def contact_info():
        if browser.current_url != about_url + 'contact-info':
            go_to(browser, about_url + 'contact-info')

        print("\nContact-info mining has been started.")
        about_session = browser.find_element_by_css_selector('#pagelet_basic .uiList').get_attribute('innerHTML')
        about_session_soup = BeautifulSoup(about_session, 'html.parser')
        query = about_session_soup.find_all('li', class_='_3pw9')
        for li in query:
            item = li.select('._50f4')[0].contents[0]
            if item == 'Gender':
                gender = gender_conversor(li.select('._2iem')[0].contents[0])
                print("gender:", gender)

            elif item == 'Birthday':
                birthday = date_conversor(li.select('._2iem')[0].contents[0])
                print("birthday:", birthday)

            elif item == 'Interested In':
                interest = interest_conversor(li.select('._2iem')[0].contents[0])
                print("interest:", interest)

            elif item == 'Languages':
                languages = (li.select('._2iem ._3l7z'))
                languagesL = []
                for language in languages:
                    languagesL.append(language.contents[0])
                print("languages:", languagesL)

            elif item == 'Religious Views':
                religious = (li.select('._2iem ._3l7z'))
                religiousL = []
                for religion in religious:
                    religiousL.append(religion.contents[0])
                print("religious:", religiousL)

    def places():
        if browser.current_url != about_url + 'living':
            go_to(browser, about_url + 'living')

        print("\nPlaces mining has been started.")
        about_session = browser.find_element_by_css_selector('.uiList').get_attribute('innerHTML')
        about_session_soup = BeautifulSoup(about_session, 'html.parser')
        query = about_session_soup.find_all('li', class_='_3pw9')

        for li in query:
            item = li.select('.fsm')[0].contents[0]
            if item.strip() == 'Current city':
                city = li.select('._2iel')[0].contents[0].contents[0]
                print('current city:', city)
            elif item.strip() == 'Hometown':
                hometown = li.select('._2iel')[0].contents[0].contents[0]
                print('hometown:', hometown)
            else:
                print(item)

            #print(li)

    def relationship():
        if browser.current_url != about_url + 'relationship':
            go_to(browser, about_url + 'relationship')

        print("\nRelationship mining has been started.")
        about_session = browser.find_element_by_css_selector('.uiList').get_attribute('innerHTML')
        about_session_soup = BeautifulSoup(about_session, 'html.parser')
        query = about_session_soup.find_all('div', class_='_4qm1')[0]

        try:
            status = query.select('div._173e')[0].contents[0]
            name = query.select('._2lzr a')[0].contents[0]
            print(name)
            print(status)

        except:
            try:
                status = query.select('._50f5')[0].contents[0]
            except:
                status = None
            print(status)



    family()
    relationship()

    #birthdate()
    #relations()


