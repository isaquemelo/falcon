from selenium.webdriver.common.keys import Keys
from bs4 import *
from time import sleep


from utilities.multitool import *
from utilities.database import *
from utilities.fb_login import login_verify
from utilities.browser_set import *


domain = 'https://facebook.com'


# CONNECT TO THE DATABASE AND SET THE CURSOR
db_connect()
datasource = db_connect()
c = datasource.cursor()


# SET UP THE BROWSER
# browser = open_browser()


def group_collector(browser, name_or_id):
    def group_data_collector(group_id_name, n_times):
        # USERS QUANTITY
        quantity_loops = n_times
        group_colector(group_id_name, quantity_loops)
    '''
        print("\nTrying to close the final procedure...")
        try:
            close(browser)
        except:
            print("The final process is already closed.")
            pass
    '''
    def group_colector(group_id, quantity_loops):
        group_url = domain + '/groups/' + group_id + '/local_members/'

        # VERIFY IF THE USER IS LOGGED
        login_verify(browser, domain)

        go_to(browser, group_url)

        for i in range(quantity_loops):
            browser.find_element_by_tag_name('html').send_keys(Keys.END)
            sleep(1)
        try:
            users = browser.find_element_by_css_selector('.fbProfileBrowserList.fbProfileBrowserListContainer').get_attribute(
            'innerHTML')
        except:
            print("The group hasn't local members.")
            return

        users_area = BeautifulSoup(users, 'html.parser')
        people_list = users_area.find_all("div", class_="_60rh")

        # COUNTS USER QUANTITY
        count = 0
        for i in range(len(people_list)):
            count += 1

            # DEFINES USERNAME
            name = people_list[i].select('div._60ri a')[0].contents
            print("name:", name[0])

            # DEFINES USER-ID
            fb_id = \
                people_list[i].select('div._60ri a')[0].get('href').replace('https://www.facebook.com/', '').replace(
                    '?fref=gm', '').replace('profile.php?id=', '').split('&')[0:1][0]
            print("fb_id:", fb_id)

            # SAVES IN THE DB
            save_user(name[0], fb_id)
            datasource.commit()

        print("counted people: ", count)

    def save_user(name, fb_id):
        send = f"INSERT INTO people VALUES (NULL, '{name}', '{fb_id}')"
        try:
            success = "The row has been saved on the DATABASE"
            c.execute(send)
            print(len(success) * "*")
            print(success)
            print(len(success) * "*", '\n')
        except:
            error = "Something went wrong. Probably the execute runned (%s) already exists in the DATABASE." % send
            print(len(error) * "*")
            print(error)
            print(len(error) * "*", '\n')

    group_data_collector(name_or_id, 2)

