from utilities.browser_set import *
from data_collector.user import user_collector
from data_collector.superficial.group import group_collector

browser = open_browser()

#fb_id = 'haline.nobrega'
#fb_id = 'henrique.cunha.pb'
#fb_id = 'cris.helder.50'
# fb_id = 'penha.dantas.351'
#fb_id = 'viviane.melo.94'
fb_id = 'davidevasconcelo.santos.5'

// Group mining
group_collector(browser, '156386671699765')

// User mining
user_collector(browser, fb_id)


print("\nTrying to close the final procedure...")
try:
    close(browser)
except:
    print("The final process is already closed.")
    pass
